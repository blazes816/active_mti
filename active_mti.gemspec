# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "active_mti/version"

Gem::Specification.new do |s|
  s.name        = "active_mti"
  s.version     = ActiveMTI::VERSION
  s.authors     = ["Tyler Smith"]
  s.email       = ["blazes816@gmail.com"]
  s.homepage    = ""
  s.summary     = %q{Allows ActiveRecord to use multiple table inheritence}
  s.description = %q{Allows ActiveRecord to use multiple table inheritence}

  s.rubyforge_project = "active_mti"

  s.files         = `git ls-files`.split("\n")
  #s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  #s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_dependency "activerecord"
  # specify any dependencies here; for example:
  # s.add_development_dependency "rspec"
  # s.add_runtime_dependency "rest-client"
end
