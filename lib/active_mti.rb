require "active_mti/version"

ActiveRecord::Base.class_eval do
  def self.inheritance(type)
    extend ActiveMTI::ClassMethods if type == :multiple_table
  end
end

module ActiveMTI
  module ClassMethods
    # Handle being inherited.  Base is our new class, so do some Ruby magic to it
    def inherited(base)
      # Get the name of our parent and child models in AR friendly downcase
      parent_class, child_class = self.name, base.name
      parent, child = parent_class.downcase, child_class.downcase
      
      # Create our association on the parent class
      belongs_to :child, :polymorphic => true
      
      # Set our child's table name, associations, and validations, and 
      # add a method_missing to delegate calls to the parent class
      set_inheritance_column :child_type
      attr_accessible :child_type
      
      base.instance_eval <<-RUBY
        set_table_name :#{child.pluralize}
        has_one :#{parent}, :as => :child, :autosave => true
        validate :ensure_child_validation
        
        def method_missing(meth, *args, &block)
          #{parent}.send(meth, *args, &block)
          rescue NoMethodError; super
        end
        
      RUBY
        
      # Start building instance methods, beginning with our parent autobuilder
      # which will be alias_method_chain'd after we eval these methods and
      # also create our validator to ensure all subclasses adhere to their
      # parent's validations
      instance_methods = <<-RUBY
        def #{parent}_with_build
          #{parent}_without_build || build_#{parent}
        end
  
        # Make sure our type column is correct, and that our parent passes validation
        def ensure_child_validation
          #{parent}.child_type = self.class.name

          unless #{parent}.valid?
            #{parent}.errors.each do |attr, message|
              errors.add(attr, message)
            end
          end
        end
      RUBY
      
      # Create accessor methods for parent attributes.  Get parent's columns
      # subtrace our default columns, then add proper accessors for each
      begin
        all = base.superclass.content_columns.map(&:name)
      rescue ActiveRecord::StatementInvalid, Mysql::Error
        all = []
      end
  
      attributes = all - ["created_at", "updated_at", "child_type"]
      attributes.map! do |attrib| 
        <<-RUBY
          def #{attrib};            #{parent}.#{attrib};          end
          def #{attrib}=(value);    #{parent}.#{attrib} = value;  end
          def #{attrib}?;           #{parent}.#{attrib}?;         end
        RUBY
      end
      
      # Append the array of method definitions
      instance_methods += attributes.join('')
      
      # Eval our instance_methods
      base.class_eval(instance_methods)
      base.alias_method_chain parent, :build
    end
  end
end

# Provides a migration helper for adding child info columns
module ActiveRecord
  module ConnectionAdapters
    class TableDefinition
      # Appends <tt>:child_id</tt> <tt>:type</tt> and to the table.
      def children(*args)
        options = args.extract_options!
        column(:child_id, :integer, options)
        column(:child_type, :string, options)
      end
    end
  end
end